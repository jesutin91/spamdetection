from flask import Flask, render_template, url_for, request
import pickle


app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')

@app.route('/predict', methods=['POST'])
def predict():
    # Load Vectorizer
    with open(r"models/vectorizer.pickle", "rb") as input_file:
        cv = pickle.load(input_file)
    # Load Model
    with open(r"models/model.pickle", "rb") as input_file:
        clf = pickle.load(input_file)
    if request.method == 'POST':
        comment = request.form['comment']
        data = [comment]
        vect = cv.transform(data).toarray()
        my_prediction = clf.predict(vect)
    return render_template('result.html', prediction=my_prediction)

if __name__ == '__main__':
    app.run(debug=True)