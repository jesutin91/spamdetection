# syntax=docker/dockerfile

# Docker image basic
FROM ubuntu:latest

# Update ubuntu
RUN apt-get update -y

# Install PIP
RUN apt-get install -y python3-pip build-essential

# LABEL about the custom image
LABEL maintainer="Jonas"
LABEL version="1.0"
LABEL description="Spam detector"

# Set Work directory to app
WORKDIR /usr/src/app

# Set environment
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

# Copy reguirements
COPY requirements.txt requirements.txt

# Install reauirements
RUN pip install -r requirements.txt

# Copy current directory files into  app in docker image
COPY . /usr/src/app

# Port
EXPOSE 80 


# Execute command
CMD ["flask", "run"]
