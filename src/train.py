
import pandas as pd
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split

#Data collection
df1 = pd.read_csv("../data/Youtube01-Psy.csv")
df2 = pd.read_csv("../data/Youtube02-KatyPerry.csv")
df3 = pd.read_csv("../data/Youtube03-LMFAO.csv")
df4 = pd.read_csv("../data/Youtube04-Eminem.csv")
df5 = pd.read_csv("../data/Youtube05-Shakira.csv")

#Concatenate all fives data in one
df  = pd.concat([df1, df2,df3,df4,df5])
df_data = df[['CONTENT', 'CLASS']]

# Features and Labels
df_x = df_data['CONTENT']
df_y = df_data.CLASS

# Extract the features with countVectorizer
corpus = df_x
cv = CountVectorizer()
X = cv.fit_transform(corpus)
X_train, X_test, y_train, y_test = train_test_split(X, df_y, test_size=0.33, random_state=42)

# Navie Bayes
clf = MultinomialNB()
clf.fit(X_train, y_train)
print(clf.score(X_test, y_test))

# Save the vectorizer
with open(r"../models/vectorizer.pickle", "wb") as output_file:
    pickle.dump(cv, output_file)
# Save Model
with open(r"../models/model.pickle", "wb") as output_file:
    pickle.dump(clf, output_file)

